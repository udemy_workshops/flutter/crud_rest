import 'dart:convert';

ProductModel productModelFromJSON(String str) =>
    ProductModel.fromJSON(json.decode(str));

String productModelToJSON(ProductModel data) => json.encode(data.toJSON());

class ProductModel {
  String id;
  String titulo;
  double valor;
  bool disponible;
  String fotoUrl;

  ProductModel({
    this.id,
    this.titulo = '',
    this.valor = 0.0,
    this.disponible = true,
    this.fotoUrl,
  });

  factory ProductModel.fromJSON(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        titulo: json["titulo"],
        valor: json["valor"].toDouble(),
        disponible: json["disponible"],
        fotoUrl: json["fotoUrl"],
      );

  Map<String, dynamic> toJSON() => {
        // "id": id,
        "titulo": titulo,
        "valor": valor,
        "disponible": disponible,
        "fotoUrl": fotoUrl,
      };
}
