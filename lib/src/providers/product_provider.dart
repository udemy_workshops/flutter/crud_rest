import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/product_model.dart';

class ProductProvider {
  final String _url = 'https://test-cacaos-co.firebaseio.com';

  Future<String> crearProducto(ProductModel product) async {
    final String url = '$_url/productos.json';
    final http.Response response =
        await http.post(url, body: productModelToJSON(product));
    if (response.reasonPhrase != 'OK') return null;
    final Map decodedData = json.decode(response.body);
    return decodedData['name'];
  }

  Future<bool> editarProducto(ProductModel product) async {
    final String url = '$_url/productos/${product.id}.json';
    final http.Response response =
        await http.put(url, body: productModelToJSON(product));
    return response.reasonPhrase == 'OK';
  }

  Future<List<ProductModel>> cargarProductos() async {
    final String url = '$_url/productos.json';
    final http.Response response = await http.get(url);
    final Map<String, dynamic> decodedData = json.decode(response.body);
    final List<ProductModel> productos = List<ProductModel>();
    if (decodedData == null) return [];
    decodedData.forEach((id, product) {
      final producto = ProductModel.fromJSON(product);
      producto.id = id;
      productos.add(producto);
    });
    return productos;
  }

  Future<bool> borrarProducto(String id) async {
    final url = '$_url/productos/$id.json';
    final http.Response response = await http.delete(url);
    return response.reasonPhrase == 'OK';
  }
}
