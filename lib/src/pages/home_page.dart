import 'package:flutter/material.dart';

import '../models/product_model.dart';
import '../providers/product_provider.dart';

// import '../bloc/provider.dart';
import 'product_page.dart';

class HomePage extends StatelessWidget {
  static const String pageName = 'home';

  final _provider = ProductProvider();

  @override
  Widget build(BuildContext context) {
    // final bloc = Provider.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Página de Inicio'),
      ),
      body: _crearListado(),
      floatingActionButton: _crearBoton(context),
    );
  }

  Widget _crearListado() {
    return FutureBuilder(
      future: _provider.cargarProductos(),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<ProductModel>> snapshot,
      ) {
        // mientras trae los datos
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }

        // datos recuperados
        final List<ProductModel> productos = snapshot.data;
        return ListView.builder(
          itemCount: productos.length,
          itemBuilder: (BuildContext context, int index) =>
              _crearItem(context, productos[index]),
        );
      },
    );
  }

  Widget _crearItem(BuildContext context, ProductModel producto) {
    return Dismissible(
      key: UniqueKey(),
      background: Container(color: Colors.red),
      // secondaryBackground: Container(color: Colors.yellow),
      child: ListTile(
        title: Text('${producto.titulo} - ${producto.valor}'),
        subtitle: Text(producto.id),
        onTap: () => Navigator.pushNamed(
          context,
          ProductPage.pageName,
          arguments: producto,
        ),
      ),
      onDismissed: (DismissDirection dismissDirection) {
        _provider.borrarProducto(producto.id);
      },
    );
  }

  FloatingActionButton _crearBoton(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () => Navigator.pushNamed(context, ProductPage.pageName),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}
