import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../utils/utils.dart' as utils;
import '../models/product_model.dart';
import '../providers/product_provider.dart';

class ProductPage extends StatefulWidget {
  static const String pageName = 'product';

  @override
  _ProductPage createState() => _ProductPage();
}

class _ProductPage extends State<ProductPage> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  ProductModel _product;
  bool _saving = false;
  File _photo;

  final ProductProvider _provider = ProductProvider();

  @override
  Widget build(BuildContext context) {
    _product = ModalRoute.of(context).settings.arguments;
    if (_product == null) _product = ProductModel();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Producto'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual),
            onPressed: _seleccionarFoto,
          ),
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _tomarFoto,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _showPicture(),
                _crearNombre(),
                _crearPrecio(),
                _crearDisponible(context),
                _crearBoton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: _product.titulo,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto',
      ),
      onSaved: (String value) => _product.titulo = value,
      validator: (String value) {
        if (value.length < 3) {
          return 'Ingrese el nombre del producto';
        }
        return null;
      },
    );
  }

  Widget _crearPrecio() {
    return TextFormField(
      initialValue: _product.valor.toString(),
      keyboardType: TextInputType.numberWithOptions(
        decimal: true,
        signed: false,
      ),
      decoration: InputDecoration(
        labelText: 'Precio',
      ),
      onSaved: (String value) => _product.valor = double.parse(value),
      validator: (String value) {
        if (utils.isNumeric(value)) return null;
        return 'Sólo números';
      },
    );
  }

  Widget _crearDisponible(BuildContext context) {
    return SwitchListTile(
      value: _product.disponible,
      title: Text('Disponible'),
      activeColor: Theme.of(context).primaryColor,
      onChanged: (bool value) => setState(() {
        _product.disponible = value;
      }),
    );
  }

  Widget _crearBoton(BuildContext context) {
    return RaisedButton.icon(
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      onPressed: _saving ? null : _submit,
    );
  }

  void _submit() {
    if (!_formKey.currentState.validate()) return;

    // cuando el formulario es valido
    _formKey.currentState.save();

    setState(() {
      _saving = true;
    });

    if (_product.id == null)
      _provider.crearProducto(_product);
    else
      _provider.editarProducto(_product);

    setState(() {
      _saving = false;
    });

    showSnackbar('Registro guardado.');

    Navigator.pop(context);
  }

  void showSnackbar(String message) {
    final snackbar = SnackBar(
      content: Text(message),
      duration: Duration(milliseconds: 1500),
    );
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Widget _showPicture() {
    if (_product.fotoUrl != null) {
      return Container();
    } else {
      if (_photo != null) {
        return Image.file(
          _photo,
          fit: BoxFit.cover,
          height: 300.0,
        );
      } else {
        return Image.asset(
          'assets/no-image.png',
          height: 300.0,
          fit: BoxFit.cover,
        );
      }
    }
  }

  Future<void> _seleccionarFoto() async {
    _photo = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    if (_photo != null) {
      // limpieza

    }
    setState(() {});
  }

  void _tomarFoto() {}
}
