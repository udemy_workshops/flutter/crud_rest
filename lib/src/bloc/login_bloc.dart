import 'dart:async';
import 'package:rxdart/rxdart.dart';

import 'validators.dart';

class LoginBLoC with Validators {
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  String get email => _emailController.value;
  String get password => _passwordController.value;

  // recuperar los datos del stream
  Stream<String> get emailStream =>
      _emailController.stream.transform(validarEMail);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);

  // validacion para el boton
  Stream<bool> get formValidStream =>
      CombineLatestStream.combine2(emailStream, passwordStream, (e, p) => true);

  // insertar valores al stream
  Function(String) get changeEMail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  void dispose() {
    _emailController?.close();
    _passwordController?.close();
  }
}
