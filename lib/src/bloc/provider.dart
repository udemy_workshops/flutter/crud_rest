import 'package:flutter/material.dart';

import 'login_bloc.dart';
export 'login_bloc.dart';

class Provider extends InheritedWidget {
  // patron singleton
  static Provider _singleton;

  factory Provider({Key key, Widget child}) {
    if (_singleton == null) _singleton = Provider._(key: key, child: child);
    return _singleton;
  }

  Provider._({Key key, Widget child}) : super(key: key, child: child);

  // bloc
  final loginBLoC = LoginBLoC();

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LoginBLoC of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<Provider>().loginBLoC;
  }
}
