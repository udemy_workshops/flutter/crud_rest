import 'package:flutter/material.dart';

import 'src/bloc/provider.dart';
import 'src/pages/login_page.dart';
import 'src/pages/home_page.dart';
import 'src/pages/product_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Validación de Formularios - Patrón BLoC',
        initialRoute: HomePage.pageName,
        routes: <String, Widget Function(BuildContext)>{
          LoginPage.pageName: (BuildContext context) => LoginPage(),
          HomePage.pageName: (BuildContext context) => HomePage(),
          ProductPage.pageName: (BuildContext context) => ProductPage(),
        },
        theme: ThemeData(primaryColor: Colors.deepPurple),
      ),
    );
  }
}
